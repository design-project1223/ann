The core of neural network is a big function that maps some input to the desired target
 value, in the intermediate step does the operation to produce the network, which is by
 multiplying weights and add bias in a pipeline scenario that does this over and over
 again. The process of training a neural network is to determine a set of parameters
 that minimize the difference between expected value and model output. This is done 
using gradient descent (aka backpropagation), which by definition comprises two steps
: calculating gradients of the loss/error function, then updating existing parameters
 in response to the gradients, which is how the descent is done. This cycle is repeated
 until reaching the minima of the loss function. This learning process can be described 
by the simple equation: W(t+1) = W(t) — dJ(W)/dW(t).



https://www.analyticsindiamag.com/6-types-of-artificial-neural-networks-currently-being-
used-in-todays-technology/


 In machine learning, we use gradient descent to update the parameters of our model.
 Parameters refer to coefficients in Linear Regression and weights in neural networks.
redite
done