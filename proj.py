import numpy as np
n=int(input("Enter the no.of inputs:"))
a = []
b = np.array([])
for i in range(n):
    print("Enter the values of SMOKING ,OBESITY,EXERCISE input values (0/1) for person:",i)
    s = int(input("Smoking habit:"))
    o = int(input("obesity:"))
    e = int(input("Exercising habit:"))
    a.append([s, o, e])
    print ("Enter the corresponding DIABETIC condition of the person:",i)
    d = int(input("Diabetic condition:"))
    b = np.append(b, d)
feature_set = np.array(a)
print("The following are the survey of ",n,"people about their smoking, obesity and exercising habits respectively:")
print(feature_set)
print("And their corresponding diabetic condition:")
labels = b.reshape(n,1)
print(labels)
C = input("ALGORITHM WORKS.......(‘p’ to proceed):")
while(C=='p'):
 np.random.seed(42)
 weights = np.random.rand(3,1)
 bias = np.random.rand(1)
 lr = 0.05
 def sigmoid(x):
    return 1/(1+np.exp(-x))


def sigmoid_der(x):
    return sigmoid(x)*(1-sigmoid(x))
 for epoch in range(20000):
    inputs = feature_set
    # feedforward step1
    XW = np.dot(feature_set, weights) + bias
  z = sigmoid(XW)
    # backpropagation step 1
    error = z - labels
    print(error.sum())
    # backpropagation step 2
    dcost_dpred = error
 dpred_dz = sigmoid_der(z)
    z_delta = dcost_dpred * dpred_dz
    inputs = feature_set.T
    weights -= lr * np.dot(inputs, z_delta)
    for num in z_delta:
        bias -= lr * num
 break
num = int(input("Enter the no.of inputs for testing:"))
for i in range(num):
  print("Give the inputs for testing diabetics(0/1):")
  s = int(input("Smoking habit:"))
  o = int(input("obesity:"))
  e = int(input("Exercising habit:"))
  single_point = np.array([s, o, e])
  result = sigmoid(np.dot(single_point, weights) + bias)
  print('Diabetic condition:')
  print(result)
  single_point = np.empty([5, 3], dtype = int)



